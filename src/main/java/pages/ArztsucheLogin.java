package pages;

import org.junit.Assert;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 * @author iuliana.uliuliuc
 * Created on 26-Oct-17
 */
public class ArztsucheLogin {
    private WebDriver driver;
    @FindBy(xpath = "//button[@accesskey='L']")
    private WebElement loginButton;

    @FindBy(id = "lifekey")
    private WebElement mailInput;
    // private By loginButton = By.xpath("//button[@accesskey='L']");

    public ArztsucheLogin(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver,this);
    }

    public void clickLoginBtn(){
        loginButton.click();
    }

    public void verifyLoginFormExists(){
        WebDriverWait wait = new WebDriverWait(driver, 10);
        wait.until(ExpectedConditions.visibilityOf(mailInput));
        Assert.assertTrue("Login form not displayed",mailInput.isDisplayed());
    }

}
