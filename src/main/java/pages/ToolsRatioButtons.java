package pages;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 * @author iuliana.uliuliuc
 * Created on 12-Oct-17
 */

public class ToolsRatioButtons {
    private WebDriver driver;

    @FindBy(id = "sex-1")
    private WebElement ratioGenderFemale;

    @FindBy(id = "sex-0")
    private WebElement ratioGenderMale;

    @FindBy(id = "profession-1")
    private WebElement autoTester;

    @FindBy(id = "profession-0")
    private WebElement manTester;

    /* private By ratioGenderFemale = By.id("sex-1");
    private By ratioGenderMale = By.id("sex-0");
    private By autoTester = By.id("profession-1");
    private By manTester = By.id("profession-0");
    */

    public ToolsRatioButtons(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    private WebElement getYearElement(Integer desiredYear){
        return driver.findElement(By.id("exp-" + desiredYear));
    }

    public void selectGender(String desiredGender) throws Exception {
        switch (desiredGender.toLowerCase()){
            case "male":
                ratioGenderMale.click();
                break;
            case "female":
                ratioGenderFemale.click();
                break;
            default: throw new Exception("Invalid gender!");
        }
    }

    public void checkSelectedGender(String desiredGender){
        WebDriverWait wait = new WebDriverWait(driver, 10);
        wait.until(ExpectedConditions.visibilityOf(ratioGenderFemale));
        if (desiredGender.equalsIgnoreCase("male")){
            Assert.assertTrue("Selected option was not male!", ratioGenderMale.isSelected());
        }
        else {
            Assert.assertTrue("Selected option was not female!", ratioGenderFemale.isSelected());
        }
    }

    public void selectYearsOfExperience(Integer nrOfYears){
        if (1 <= nrOfYears && nrOfYears <= 7) {
            WebElement ratioYearsExperience = getYearElement(nrOfYears - 1);
            if (!ratioYearsExperience.isSelected()){
                ratioYearsExperience.click();
            }
        }
        else {
            System.out.println("Not correct number of years!");
        }
    }

    public void checkSelectedYear(Integer nrOfYears) throws Exception {
        if (1 <= nrOfYears && nrOfYears <= 7) {
            WebElement ratioYearsExperience = getYearElement(nrOfYears - 1);
            Assert.assertTrue("Selected number for years of experience is not the desired one!", ratioYearsExperience.isSelected());
        }
        else {
            throw new Exception("Number of years is not correct!");
            // System.out.println("Not correct number of years!");
        }
    }

    public void selectTesterType(String testerType) throws Exception {
        switch (testerType.toLowerCase()){
            case "manual":
                manTester.click();
                break;
            case "automation":
                autoTester.click();
                break;
            default:
                throw new Exception("Invalid tester type!");
        }
    }

    public void checkSelectedTesterType(String testerType) throws Exception {
        // WebElement autoTester = driver.findElement(By.id("profession-1"));
        // WebElement manTester = driver.findElement(By.id("profession-0"));
        switch (testerType.toLowerCase()){
            case "manual":
                Assert.assertTrue("Manual tester is not selected!", manTester.isSelected());
                break;
            case "automation":
                Assert.assertTrue("Automation tester is not selected!", autoTester.isSelected());
                break;
            default:
                throw new Exception("Invalid tester type!");
        }
    }

}
