import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

import java.util.List;

public class TestImdb {

    private WebDriver driver;

    @Before
    public void setup(){
        System.setProperty("webdriver.chrome.driver", "c:\\BrowserDrivers\\chromedriver.exe");
        driver = new ChromeDriver();
        driver.manage().window().maximize();
    }

    @Test
    public void findMovie(){
        String url = "http://www.imdb.com/";
        driver.get(url);
        String movieName= "planet of the apes";
        enterText(movieName);
        selectFromDropDown("Titles");
        clickSearch();
        driver.findElement(By.linkText("Exact title matches")).click();
        verifyResults(movieName);
    }

    private  void verifyResults(String movieName){
        List<WebElement> resultsList = driver.findElements(By.className("result_text"));
        for (WebElement elem : resultsList) {
            String foundMovie = elem.getText().toLowerCase();
            Assert.assertTrue(foundMovie.contains(movieName));
        }
    }

    private void enterText(String textEntered){
        WebElement navBar = driver.findElement(By.id("navbar-query"));
        navBar.sendKeys(textEntered);
    }

    private void selectFromDropDown(String movieType){
        WebElement dropDownMovieTypes = driver.findElement(By.id("quicksearch"));
        Select dropDownMovie = new Select(dropDownMovieTypes);
        dropDownMovie.selectByVisibleText(movieType);
    }

    private void clickSearch(){
        WebElement searchButton = driver.findElement(By.id("navbar-submit-button"));
        searchButton.click();
    }

    @After
    public void close(){
        driver.close();
    }

}
