/**
 * @author iuliana.uliuliuc
 * Updated on 11.10.2017
 */

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

import java.util.List;

public class TestToolsQA {
    private WebDriver driver;

    @Before
    public void setup(){
        System.setProperty("webdriver.chrome.driver", "c:\\BrowserDrivers\\chromedriver.exe");
        driver = new ChromeDriver();
        driver.manage().window().maximize();
    }

    @Test
    public void testPageToolsQA() throws Exception {
        String url = "http://toolsqa.com/automation-practice-form/";
        driver.get(url);
        selectGender("male");
        checkSelectedGender("male");
        selectYearsOfExperience(1);
        checkSelectedYear(1);
        WebElement ratioYearsExperience = driver.findElement(By.id("exp-0"));
        ratioYearsExperience.click();
        try {
            Thread.sleep(3000);
        }
        catch (InterruptedException e){
            e.printStackTrace();
        }
        selectAllYears();
        try {
            Thread.sleep(7000);
        }
        catch (InterruptedException e){
            e.printStackTrace();
        }
    }

    @Test
    public void testNameFields() throws Exception {
        String url = "http://toolsqa.com/automation-practice-form/";
        driver.get(url);
        enterFirstName("ana");
        checkFirstName("ana");
        enterLastName("popescu");
        checkLastName("popescu");
        selectTesterType("manual");
        checkSelectedTesterType("manual");
        enterDate("11.11.2013");
        checkEnteredDate("11.11.2013");
        try {
            Thread.sleep(3000);
        }
        catch (InterruptedException e){
            e.printStackTrace();
        }
    }

    @Test
    public void testOthers() throws Exception {
        String url = "http://toolsqa.com/automation-practice-form/";
        driver.get(url);
        selectContinent("Europe");
        checkSelectedContinent("Europe");
        selectCommand("Wait Commands");
        checkSelectedCommand("Wait Commands");
        try {
            Thread.sleep(3000);
        }
        catch (InterruptedException e){
            e.printStackTrace();
        }
    }

    @Test
    public void clickOnLink(){
        String url = "http://toolsqa.com/automation-practice-form/";
        driver.get(url);
        driver.findElement(By.xpath("//*[@id=\"content\"]/form/fieldset/div[1]/a[2]")).click();
        Assert.assertTrue(driver.findElement(By.xpath("//*[@id=\"content\"]/h1")).getText().equalsIgnoreCase("Automation Practice Table"));
        checkValueFromTable(1,1,"UAE");
        try {
            Thread.sleep(3000);
        }
        catch (InterruptedException e){
            e.printStackTrace();
        }
    }

    private void checkValueFromTable(Integer rowNr, Integer colNr, String expectedValue){
        Integer rowSize = driver.findElements(By.xpath("//*[@id=\"content\"]/table/tbody/tr")).size();
        Integer colSize = driver.findElements(By.xpath("//*[@id=\"content\"]/table/tbody/tr[1]/td")).size();
        if (1 <= rowNr && rowNr <= rowSize && 1 <= colNr && colNr <= colSize){
            WebElement cellToCheck = driver.findElement(By.xpath("//*[@id=\"content\"]/table/tbody/tr[" + rowNr +"]/td[" + colNr + "]"));
            System.out.println("Cell has value: " + cellToCheck.getText());
            Assert.assertTrue("Cell value is not the desired one", cellToCheck.getText().equalsIgnoreCase(expectedValue));
        }
    }

    private void selectCommand(String desiredCommand){
        WebElement seleniumCommands = driver.findElement(By.id("selenium_commands"));
        Select listOfCommands = new Select(seleniumCommands);
        if (!seleniumCommands.getAttribute("value").equalsIgnoreCase(desiredCommand)){
            listOfCommands.selectByVisibleText(desiredCommand);
        }
    }

    private void checkSelectedCommand(String desiredCommand){
        WebElement seleniumCommands = driver.findElement(By.id("selenium_commands"));
        Assert.assertTrue("Selected command is not the desired one!", seleniumCommands.getAttribute("value").equalsIgnoreCase(desiredCommand));
    }

    private void selectContinent(String desiredContinent){
        WebElement continentInput = driver.findElement(By.id("continents"));
        Select dropDownContinent = new Select(continentInput);
        if (!continentInput.getAttribute("value").equalsIgnoreCase(desiredContinent)){
            dropDownContinent.selectByVisibleText(desiredContinent);
        }
    }

    private void checkSelectedContinent(String desiredContinent){
        WebElement continentInput = driver.findElement(By.id("continents"));
        Assert.assertTrue("Select continent is not desired one!", continentInput.getAttribute("value").equalsIgnoreCase(desiredContinent));
    }

    private void enterDate(String desiredDate){
        WebElement dateInput = driver.findElement(By.xpath("//*[@id='datepicker']"));
        dateInput.clear();
        dateInput.sendKeys(desiredDate);
    }

    private void checkEnteredDate(String desiredDate){
        WebElement dateInput = driver.findElement(By.xpath("//*[@id=\"datepicker\"]"));
        Assert.assertTrue("Entered date is not the desired one!", dateInput.getAttribute("value").equalsIgnoreCase(desiredDate));
    }


    private void selectTesterType(String testerType) throws Exception {
        WebElement autoTester = driver.findElement(By.id("profession-1"));
        WebElement manTester = driver.findElement(By.id("profession-0"));
        switch (testerType.toLowerCase()){
            case "manual":
                manTester.click();
                break;
            case "automation":
                autoTester.click();
                break;
            default:
                throw new Exception("Invalid tester type!");
        }
    }

    private void checkSelectedTesterType(String testerType) throws Exception {
        WebElement autoTester = driver.findElement(By.id("profession-1"));
        WebElement manTester = driver.findElement(By.id("profession-0"));
        switch (testerType.toLowerCase()){
            case "manual":
                Assert.assertTrue("Manual tester is not selected!", manTester.isSelected());
                break;
            case "automation":
                Assert.assertTrue("Automation tester is not selected!", autoTester.isSelected());
                break;
            default:
                throw new Exception("Invalid tester type!");
        }
    }

    private void enterFirstName(String givenFirstName){
        WebElement firstName = driver.findElement(By.name("firstname"));
        firstName.clear();
        firstName.sendKeys(givenFirstName);
    }

    private void checkFirstName(String givenFirstName){
        WebElement firstName = driver.findElement(By.name("firstname"));
        Assert.assertTrue("First name is not the desired one!", firstName.getAttribute("value").equalsIgnoreCase(givenFirstName));
    }

    private void enterLastName(String givenLastName){
        WebElement lastName = driver.findElement(By.name("lastname"));
        lastName.clear();
        lastName.sendKeys(givenLastName);
    }

    private void checkLastName(String givenLastName){
        WebElement lastName = driver.findElement(By.name("lastname"));
        Assert.assertTrue("Last name is not the desired one!", lastName.getAttribute("value").equalsIgnoreCase(givenLastName));
    }

    private void selectGender(String desiredGender) throws Exception {
        WebElement ratioGenderFemale = driver.findElement(By.id("sex-1"));
        WebElement ratioGenderMale = driver.findElement(By.id("sex-0"));
        switch (desiredGender.toLowerCase()){
            case "male":
                ratioGenderMale.click();
                break;
            case "female":
                ratioGenderFemale.click();
                break;
            default: throw new Exception("Invalid gender!");
        }
    }

    private void checkSelectedGender(String desiredGender){
        WebElement ratioGenderFemale = driver.findElement(By.id("sex-1"));
        WebElement ratioGenderMale = driver.findElement(By.id("sex-0"));
        if (desiredGender.equalsIgnoreCase("male")){
            Assert.assertTrue("Selected option was not male!", ratioGenderMale.isSelected());
        }
        else {
            Assert.assertTrue("Selected option was not female!", ratioGenderFemale.isSelected());
        }
    }

    private void selectYearsOfExperience(Integer nrOfYears){
            if (1 <= nrOfYears && nrOfYears <= 7) {
                WebElement ratioYearsExperience = driver.findElement(By.id("exp-"+(nrOfYears - 1)));
                if (!ratioYearsExperience.isSelected()){
                    ratioYearsExperience.click();
                }
            }
            else {
                System.out.println("Not correct number of years!");
            }
    }

    private void checkSelectedYear(Integer nrOfYears) throws Exception {
        if (1 <= nrOfYears && nrOfYears <= 7) {
            WebElement ratioYearsExperience = driver.findElement(By.id("exp-"+(nrOfYears - 1)));
            Assert.assertTrue("Selected number for years of experience is not the desired one!", ratioYearsExperience.isSelected());
        }
        else {
            throw new Exception("Number of years is not correct!");
            // System.out.println("Not correct number of years!");
        }
    }

    private void selectAllYears(){
        List<WebElement> yearsOfExp = driver.findElements(By.name("exp"));
        for (WebElement elem : yearsOfExp) {
            if (!elem.isSelected()) {
                elem.click();
                if (elem.isSelected()) {
                    System.out.println("Selected elem: " + elem.getAttribute("value"));
                }
            }
            else {
                System.out.println("Elem: " + elem.getAttribute("value") + " was already selected!");
            }
        }
    }

    @After
    public void close(){
        driver.close();
    }
}
