import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import pages.ArztsucheLogin;

/**
 * @author iuliana.uliuliuc
 * Created on 26-Oct-17
 */
public class TestArztsucheLogin {
    private WebDriver driver;

    @Before
    public void setup(){
        System.setProperty("webdriver.chrome.driver", "c:\\BrowserDrivers\\chromedriver.exe");
        driver = new ChromeDriver();
        //String url = "https://sdevko75.cgm.ag/arztsuche/landingpage";
        String url = "https://AuTS:appointmentBooking16!!@dev.cgmlife.com/arztsuche/landingpage";
        driver.get(url);
        // Alert alert = driver.switchTo().alert();
        //ExpectedCondition alertShown = ExpectedConditions.alertIsPresent();
        /*WebDriverWait wait = new WebDriverWait(driver, 150);
        wait.until(ExpectedConditions.alertIsPresent());
        Alert alert = driver.switchTo().alert();
        alert.authenticateUsing(new UserAndPassword("AuTS", "appointmentBooking16!!"));
        driver.switchTo().defaultContent(); */
        driver.manage().window().maximize();
    }

    @Test
    public void testGender()throws Exception{
        //driver.manage().window().maximize();
        ArztsucheLogin arztsucheLogin = new ArztsucheLogin(driver);
        //Thread.sleep(3000);
        arztsucheLogin.clickLoginBtn();
        //Thread.sleep(5000);
        arztsucheLogin.verifyLoginFormExists();
        Thread.sleep(3000);
    }

    @After
    public void close(){
        driver.close();
    }
}
