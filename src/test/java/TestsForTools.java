import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import pages.ToolsRatioButtons;

/**
 * @author iuliana.uliuliuc
 * Created on 12-Oct-17
 */

public class TestsForTools {
    private WebDriver driver;

    @Before
    public void setup(){
        System.setProperty("webdriver.chrome.driver", "c:\\BrowserDrivers\\chromedriver.exe");
        driver = new ChromeDriver();
        driver.manage().window().maximize();
    }

    @Test
    public void testGender()throws Exception{
        ToolsRatioButtons toolsRatioButtons = new ToolsRatioButtons(driver);
        String url = "http://toolsqa.com/automation-practice-form/";
        driver.get(url);
        toolsRatioButtons.selectGender("male");
        toolsRatioButtons.checkSelectedGender("male");
        Thread.sleep(3000);
    }

    @Test
    public void testYears()throws Exception{
        ToolsRatioButtons toolsRatioButtons = new ToolsRatioButtons(driver);
        String url = "http://toolsqa.com/automation-practice-form/";
        driver.get(url);
        toolsRatioButtons.selectYearsOfExperience(2);
        toolsRatioButtons.checkSelectedYear(2);
        Thread.sleep(3000);
    }

    @After
    public void close(){
        driver.close();
    }
}
